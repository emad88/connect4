# Connect four game #



This application uses Spring boot, AngularJS, and bootstrap.

The application is written in intellij in case you want to open and check the code.

After running the application, just browse "localhost:8080"

Or Alternatively you can check the app at this address:

## [connect4](https://connect4slots.herokuapp.com/) ##


Since the data is initiated at the UI, and backend does the calculations, multiple tabs can be opened and play the game at the same time.



Hope you enjoy.