package serviceTest;

import model.Matrix;
import org.junit.Before;
import org.junit.Test;
import service.Connect4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by emadnikkhouy on 11/09/16.
 */
public class Connect4Test {

    private Connect4 connect4;
    private Matrix matrix;
    private List<List<Integer>> board;

    private List<Integer> lastMove;

    @Before
    public void setUp() {
        connect4 = new Connect4();
        board = new ArrayList<>();
    }


    @Test
    public void TestGetWinner() {
        initializeValues();
        board = asList(
                asList(0, 0, 0, 0, 0, 0, 0),
                asList(0, 0, 0, 0, 0, 0, 0),
                asList(0, 0, 2, 0, 1, 0, 0),
                asList(0, 0, 1, 2, 1, 0, 0),
                asList(0, 0, 1, 1, 2, 1, 1),
                asList(0, 0, 1, 2, 2, 2, 1));
        lastMove.add(2);
        lastMove.add(2);

        setMatrix();
        connect4.setBoard(matrix);
        assertEquals("player 2 suppose to be the winner", 2, connect4.getWinner(matrix));

    }

    @Test
    public void TestGetWinner2() {
        initializeValues();
        board = asList(
                asList(0, 0, 0, 0, 0, 0, 0),
                asList(0, 0, 0, 0, 0, 0, 0),
                asList(0, 0, 0, 0, 1, 0, 0),
                asList(0, 0, 1, 2, 1, 0, 0),
                asList(0, 0, 2, 1, 1, 1, 1),
                asList(0, 0, 1, 2, 2, 2, 1));
        lastMove.add(4);
        lastMove.add(4);

        setMatrix();
        connect4.setBoard(matrix);
        assertEquals("player 1 suppose to be the winner", 1, connect4.getWinner(matrix));

    }

    @Test
    public void TestGetWinner3() {
        initializeValues();
        board = asList(
                asList(0, 0, 0, 0, 0, 0, 0),
                asList(0, 0, 0, 0, 0, 0, 0),
                asList(0, 0, 0, 0, 1, 0, 0),
                asList(0, 0, 1, 2, 1, 0, 0),
                asList(0, 0, 2, 1, 0, 1, 1),
                asList(0, 0, 1, 2, 2, 2, 1));
        lastMove.add(2);
        lastMove.add(4);

        setMatrix();
        connect4.setBoard(matrix);
        assertEquals("player 1 suppose to be the winner", 0, connect4.getWinner(matrix));

    }

    private void initializeValues() {
        matrix = new Matrix();
        lastMove = new ArrayList<>();
    }

    private void setMatrix() {
        matrix.setMatrix(board);
        matrix.setLastMove(lastMove);

    }


    private static <T> List<T> asList(T... items) {
        List<T> list = new ArrayList<>();
        for (T item : items) {
            list.add(item);
        }

        return list;
    }


}
