app.controller("controller", function ($scope, $http, $window, $log) {

    var firstPlayer;
    var lastPlayer;
    var lastMovePosition;
    var jsonData;
    var moveCounter;


    init();

    function init() {
        $scope.data =
            [[0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0]];
        firstPlayer = 1;
        lastPlayer = 0;
        moveCounter = 0;
        $scope.winner = 0;
        $scope.player = 1;
        $scope.error = "";

    }

    $scope.reset = function () {
        init();
    };

    $scope.selectPosition = function ($index) {

        if ($scope.winner == 0) {
            for (var i = 5; i >= 0; i--) {
                if ($scope.data[i][$index] == 0) {
                    $scope.data[i][$index] = (lastPlayer == firstPlayer) ? 2 : 1;
                    lastPlayer = $scope.data[i][$index];
                    lastMovePosition = [i, $index];
                    moveCounter++;
                    toJson();
                    displayNextPlayer();
                    post();
                    break;
                }
            }
        }

        function toJson() {
            jsonData = {
                matrix: $scope.data,
                lastMove: lastMovePosition,
                moveCounter: moveCounter
            };
        }

        $scope.haveWinner = function () {
            return $scope.winner == 1 || $scope.winner == 2 || $scope.winner == 3;
        };

        function displayNextPlayer() {
            $scope.player = (lastPlayer == firstPlayer) ? 2 : 1;
        }

        function post() {

            $http.post('status', jsonData)
                .then(
                    function (response) {
                        var winner = response.data;
                        if (winner == 1 || winner == 2) {
                            $scope.winner = winner;
                            $scope.winnerMsg = "Player " + winner + " is the winner!";
                        } else if (winner == 3) {
                            $scope.winner = winner;
                            $scope.winnerMsg = "It's Draw";
                        }
                    },
                    function (response) {
                        $scope.error = "Something Went Wrong, Try Later!";
                    }
                );
        }
    };


    $scope.about = function () {
        $window.location.href = '/about.html';
    };
});


app.controller("aboutController", function ($scope, $window) {
    $scope.home = function () {
        $window.location.href = '/index.html';
    };
    $scope.wiki = function () {
        $window.open('https://en.wikipedia.org/wiki/Connect_Four')
    }
});