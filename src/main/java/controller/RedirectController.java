package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by emadnikkhouy on 12/09/16.
 */
@Controller
public class RedirectController {
    @RequestMapping("/")
    private String index(){
        return "redirect:/index.html";
    }
}
