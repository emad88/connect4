package controller;

import model.Matrix;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import service.Connect4;

import javax.validation.Valid;
import java.util.concurrent.Callable;

/**
 * Created by emadnikkhouy on 07/09/16.
 */
@RestController
public class GameController {

    @Autowired
    private Connect4 connect4;


    @RequestMapping(value = "/status", method = RequestMethod.POST, consumes="application/json")
    private Callable<Integer> getStatus(@Valid @RequestBody final Matrix board){

        connect4.setBoard(board);
        Callable<Integer> asyncResult = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return connect4.getWinner(board);
            }
        };

        return asyncResult ;

    }

}
