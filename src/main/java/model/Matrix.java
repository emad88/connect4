package model;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by emadnikkhouy on 07/09/16.
 */
@Component
public class Matrix {


    @NotNull
    @NotEmpty
    public List<List<Integer>> matrix;
    @NotNull
    @NotEmpty
    public List<Integer> lastMove;
    @NotNull
    public int moveCounter;

    public List<List<Integer>> getMatrix() {
        return matrix;
    }

    public void setMatrix(List<List<Integer>> matrix) {
        this.matrix = matrix;
    }

    public List<Integer> getLastMove() {
        return lastMove;
    }

    public void setLastMove(List<Integer> lastMove) {
        this.lastMove = lastMove;
    }

    public int getMoveCounter() {
        return moveCounter;
    }

    public void setMoveCounter(int moveCounter) {
        this.moveCounter = moveCounter;
    }
}
