package com.gluck;

import configuration.SpringConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by emadnikkhouy on 07/09/16.
 */
public class Connect4Application {


    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(SpringConfiguration.class);
        builder.headless(false);
        ConfigurableApplicationContext applicationContext = builder.run(args);
    }
}
