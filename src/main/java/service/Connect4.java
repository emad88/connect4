package service;

import model.Matrix;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Created by emadnikkhouy on 10/09/16.
 */
@Component
public class Connect4 {
    private int[][] board;
    private boolean[] flag;
    private final int ROW = 6;
    private final int COL = 7;
    private final int MAX_MOVES = 42;

    {
        board = new int[ROW][COL];
        flag = new boolean[8];

    }

    public void setBoard(Matrix board) {
        for (int i = 0; i < ROW; i++)
            for (int j = 0; j < COL; j++)
                this.board[i][j] = board.getMatrix().get(i).get(j);
    }


    public int getWinner(Matrix board) {
        int col = 1, row = 1, diagLeft = 1, diagRight = 1;
        Arrays.fill(flag, true);
        int lastMoveI = board.getLastMove().get(0);
        int lastMoveJ = board.getLastMove().get(1);
        int player = board.getMatrix().get(lastMoveI).get(lastMoveJ);

        for (int z = 1; z < 4; z++) {
            col += insideBoard(lastMoveI - z, lastMoveJ) && this.board[lastMoveI - z][lastMoveJ] == player &&
                    flag[0] ? 1 : switchFlag(0); // column up

            col += insideBoard(lastMoveI + z, lastMoveJ) && this.board[lastMoveI + z][lastMoveJ] == player &&
                    flag[1] ? 1 : switchFlag(1); // column down

            row += insideBoard(lastMoveI, lastMoveJ - z) && this.board[lastMoveI][lastMoveJ - z] == player &&
                    flag[2] ? 1 : switchFlag(2); // row left

            row += insideBoard(lastMoveI, lastMoveJ + z) && this.board[lastMoveI][lastMoveJ + z] == player &&
                    flag[3] ? 1 : switchFlag(3); // row right

            diagLeft += insideBoard(lastMoveI - z, lastMoveJ - z) && this.board[lastMoveI - z][lastMoveJ - z] == player &&
                    flag[4] ? 1 : switchFlag(4); // diagonal left up \

            diagLeft += insideBoard(lastMoveI + z, lastMoveJ + z) && this.board[lastMoveI + z][lastMoveJ + z] == player &&
                    flag[5] ? 1 : switchFlag(5); // diagonal down right \

            diagRight += insideBoard(lastMoveI - z, lastMoveJ + z) && this.board[lastMoveI - z][lastMoveJ + z] == player &&
                    flag[6] ? 1 : switchFlag(6); // diagonal up right /

            diagRight += insideBoard(lastMoveI + z, lastMoveJ - z) && this.board[lastMoveI + z][lastMoveJ - z] == player &&
                    flag[7] ? 1 : switchFlag(7); // diagonal down left /
        }
        return col >= 4 || row >= 4 || diagLeft >= 4 || diagRight >= 4 ? player : isDraw(board.getMoveCounter());
    }

    private int isDraw(int moveCounter) {
        if (moveCounter == MAX_MOVES)
            for (int i = 0; i < ROW; i++)
                for (int j = 0; j < COL; j++)
                    if (board[i][j] != 0)
                        return 3; // 3 is the draw

        return 0;  // there are still moves
    }

    private int switchFlag(int position) {
        flag[position] = false;
        return 0;
    }

    private boolean insideBoard(int i, int j) {
        return !(i > 5 || i < 0 || j > 6 || j < 0);
    }


}
